import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var listOfProducts: [Products] = []
    var viewModel: ViewModel?
    var product: Products?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicatorView.startAnimating()
        self.view.addSubview(self.activityIndicatorView)
        viewModel = ViewModel()
        viewModel?.getProducts()
        bindOutlets()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        self.activityIndicatorView.isHidden = true
    }
    
    func bindOutlets() {
        viewModel?.listOfProducts.bind({ (result) in
            DispatchQueue.main.async {
                if result != nil {
                    if let result = result.map({$0}) {
                        self.listOfProducts.append(contentsOf: result)
                        self.collectionView.reloadData()
                    }
                }
            }
        })
    }
   
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        let products = listOfProducts.map({$0})
        cell.configure(with: products[indexPath.item])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "DetailInfoViewController") as! DetailInfoViewController
        
        let nameArray = listOfProducts.map({$0.name})
        let priceArray = listOfProducts.map({$0.price})
        let pictureArray = listOfProducts.map({$0.image})
        
        controller.name = nameArray[indexPath.row]!
        controller.price = priceArray[indexPath.row]!
        controller.image = pictureArray[indexPath.row]!
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side: CGFloat = (collectionView.frame.size.width - 10) / 2
        return CGSize(width: side, height: side)
    }
}

