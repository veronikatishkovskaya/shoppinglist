import UIKit

class DetailInfoViewController: UIViewController {
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    let manager = FileManagerClass()
    var viewModel: ViewModel?
    var listOfProducts: [Products] = []
    var name = ""
    var price = 0
    var image = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productName.text = name
        productPrice.text = "\(price)р."
        getProductImage()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getProductImage() {
        guard let url = URL(string: image) else { return }
        guard let data = try? Data(contentsOf: url) else { return }
        self.productImage.image = UIImage(data: data)
    }
}
