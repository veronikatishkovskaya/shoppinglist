import Foundation

class Products {
    
    var product_id: String?
    var name: String?
    var price: Int?
    var image: String?
    
    init(product_id: String?, name: String?, price: Int?, image: String?) {
        
        self.product_id = product_id
        self.name = name
        self.price = price
        self.image = image
    }
    
}
