import Foundation
import UIKit

class ViewModel: NSObject {
    
    var listOfProducts = Bindable<[Products]?>(nil)
    var productsInfo : [Products] = []
    
    override init() {
        super.init()
    }
    
    func getProducts() {
        Manager.shared.getListOfProducts { (listOfProducts) in
            self.productsInfo = listOfProducts
            self.listOfProducts.value = listOfProducts
        }
    }
}


