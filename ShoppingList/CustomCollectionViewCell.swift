import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    func configure(with object: Products) {
        productView.layer.borderWidth = 1
        productName.text = object.name
        
        guard let price = object.price else { return }
        productPrice.text = "\(price)р."
        
        guard let url = URL(string: "\(object.image ?? "no image")") else { return }
        guard let data = try? Data(contentsOf: url) else { return }
        self.productImage.image = UIImage(data: data)
    }
}


