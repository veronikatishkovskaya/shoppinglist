import Foundation

class Manager {
    
    static let shared = Manager()
    var objects = Objects()
    var listOfProducts : [Products] = []
    
    private init() {}
    
    func getListOfProducts (completion: @escaping (([Products]) -> ())) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "s3-eu-west-1.amazonaws.com"
        urlComponents.path = "/developer-application-test/cart/list"
        
        var request = URLRequest(url: (urlComponents.url!))
        request.httpMethod = "GET"
        
        let task = URLSession(configuration: .default)
        
        task.dataTask(with: request) { (data, response, error) in
            if error == nil {
                
                let decoder = JSONDecoder()
                var decoderObjects: Objects?
                if data != nil {
                    decoderObjects = try? decoder.decode(Objects.self, from: data!)
                }
                self.objects = decoderObjects!
                self.createListOfProducts()
                completion(self.listOfProducts)
                
            } else {
                print(error as Any)
            }
        } .resume()
    }
    
    func createListOfProducts() {
        guard let products = objects.products else { return }
        for product in products {
            let productId = product.product_id
            let name = product.name
            let price = product.price
            let image = product.image
            
            let list = Products(product_id: productId, name: name, price: price, image: image)
            listOfProducts.append(list)
        }
    }
    
}
