import Foundation

class Objects: Codable {
    var products: [ProductInfo]?
}

class ProductInfo: Codable {
    var product_id: String?
    var name: String?
    var price: Int?
    var image: String?
}
